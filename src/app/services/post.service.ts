import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Post } from '../models/Post';

const httpOptions = { ////// 
  headers: new HttpHeaders({'Content-type' : 'application/json'})
}; 

@Injectable({
  providedIn: 'root'
})

// this class deals with communicating with the API
export class PostService {

  postsUrl: string = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private http:HttpClient) { }

  getPosts () : Observable<Post[]> {
    return this.http.get<Post[]>(this.postsUrl);
  }

  savePost (post: Post): Observable<Post> {
    return this.http.post<Post>(this.postsUrl,post,httpOptions);
  }

  //  we want to update the post we are "editing", so we need the ID of the post
  updatePost (post: Post): Observable<Post> {
    const url = `${this.postsUrl}/${post.id}`; // API url + post's ID
    return this.http.put<Post>(url,post,httpOptions);
  }

  getPost (id: number): Observable<Post> { // Observable cus subscribing
    const url = `${this.postsUrl}/${id}`; // API url + post's ID
    return this.http.get<Post>(url);
  }

              //  takes in a post or a number
  deletePost (post: Post | number): Observable<Post> {
    const id = typeof post === 'number' ? post : post.id; /// if the post is a number, we store the post, otherwise we just store the id
    const url = `${this.postsUrl}/${id}`;

    return this.http.delete<Post>(url,httpOptions);
  }

  // post, put and delete are requests btw
}
