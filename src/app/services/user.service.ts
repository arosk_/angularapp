import { Injectable } from '@angular/core';

import { User } from '../models/User';


import { Observable } from 'rxjs';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  users : User [];

  constructor() {

    this.users = [
      {
        firstName : "John",
        lastName : "Wick",
        email : "JohnWick@live.com",
        // image : 'http://lorempixel.com/600/600/cats/3',
        isActive : false,
        registered : new Date ('11/22/2011 01:00:00'),
        hide : true
      }, 
      {
        firstName : "Pup",
        lastName : "Wick",
        email : "pupWick@live.com",
        // image : 'http://lorempixel.com/600/600/cats/8',
        isActive : true,
        registered : new Date ('12/22/2015 15:00:00'),
        hide : true

      },
      {
        firstName : "Steve",
        lastName : "Ride",
        email : "SteveRide@gmail.com",
        // image : 'http://lorempixel.com/600/600/cats/4',
        isActive : false,
        registered : new Date ('1/9/2018 02:30:00'),
        hide : true
      }
    ];
   }

   addUsers (user : User) {
     this.users.unshift(user);
   }

  //  getUsers () : User [] {
  //    return this.users;
  //  }

   // when using Observable for getUsers ()
   getUsers () : Observable<User []> {
    return of(this.users);
  }
}
