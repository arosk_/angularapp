export interface User {
    firstName : string,
    lastName : string,
    email : string,
    image? : string,
    isActive? : boolean,
    registered? : Date, // because going to be a date
    hide? : boolean
}