import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

import { Post } from '../../models/Post';  

import { PostService } from '../../services/post.service';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

  
  @Output() newPost: EventEmitter<Post> = new EventEmitter();
  @Output() updatedPost: EventEmitter<Post> = new EventEmitter();
  @Input() currentPost: Post;
  @Input() isEdit: boolean = false;

  constructor(private postService : PostService) { }

  ngOnInit() {
  }

  addPost (title,body) {
    if (!title || !body) { // if neither or both have not been entered
      alert ("Please enter post content!");
    } else {
      this.postService.savePost({title,body} as Post).subscribe(post => {
        this.newPost.emit(post);
      })
    }
  }

  // note that this is using the updatePost() method from the post.service.ts
  updatePost () {
    this.postService.updatePost(this.currentPost).subscribe(post => {
      this.updatedPost.emit(post);
      this.isEdit = false; // stop editing / is no longer being edited
    });
  }


  

}
