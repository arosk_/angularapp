/**
 * User Component - 30 Aug 19
 */
import { Component, OnInit } from '@angular/core';
import { User } from '../../models/User';

@Component({
    selector: 'app-user', // app- always, then the name of component 'user'
    templateUrl: './user.component.html',
    styleUrls:['./user.component.css']

    // styles: [`
    //     h3 {
    //         color: blue;
    //     }
    // `]

    // template: '<h2>Sora Khan from user.component.ts</h2>' 
    // not really want to have template^ here
    // good practice to have it in a separate file
    // create 'user.component.html' and use templateUrl here instead 
        // that links to ^
    
})

// to access class from outside, use 'export'
export class UserComponent implements OnInit{
    user : User;

    constructor () {
        
    }

    ngOnInit () {
        this.user = {
            firstName : "Sora",
            lastName : "Khan",
            email : "sora.khan@gmail.com"
            // age : 22,
            // address : {
            //     street: '100 Some St',
            //     city: 'The City',
            //     state: 'NSW'
            // }
        }
    }

  

}
// third and usual way to deal with classes
// with user defined - is to put it in an interface
// but we put it in a different area (under 'models' folder)
/*

interface User {
    firstName : string,
    lastName : string,
    age : number,
    address : {
        street : string,
        city : string,
        state : string
    }
}*/


/*
        // second way 
        user : {
            firstName : string,
            lastName : string,
            age : number,
            address : {
                street : string,
                city : string,
                state : string
            }
        }

        constructor () {
            this.user = {
                firstName : "Sora",
                lastName : "Khan",
                age : 22,
                address : {
                    street: '100 Some St',
                    city: 'The City',
                    state: 'NSW'
                }
            }
        }
    */



    /*
        // First way we did this
    firstName : string;
    lastName : string;
    age : number;
    address;

    constructor () {
        this.firstName = "Sora";
        this.lastName = "Khan";
        this.age = 23;
        this.address = {
            street: '100 Some St',
            city: 'The City',
            state: 'NSW'
        };
        this.sayWelcome();
        this.hasBirthday();
    }

    sayWelcome () {
        console.log(`Welcome, ${this.firstName}.`);
    }

    hasBirthday () {
        this.age += 1;
        console.log(`You are now ${this.age} years old.`);
    }*/
