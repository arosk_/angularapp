import { Component, OnInit } from '@angular/core';
// these 3 imports below ActivatedRoute, Location, PostService are in params of constructor
import { ActivatedRoute } from '@angular/router';
import { PostService } from 'src/app/services/post.service';

import { Post } from '../../models/Post'; // to be able to retrieve Post's attribute like id

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {

  post : Post;

  constructor( // these are so that we can obtain the id of a post!
    private route: ActivatedRoute, // imported
    private postService: PostService// imported
  ) { }

  ngOnInit() {
    const id = + this.route.snapshot.paramMap.get('id');
    this.postService.getPost(id).subscribe( post => {
      this.post = post;
    });
  }

}
