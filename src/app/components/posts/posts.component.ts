import { Component, OnInit } from '@angular/core';

import { Post } from '../../models/Post';  

import { PostService } from '../../services/post.service';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts : Post[];

  // form post..
  currentPost: Post = { // attributes match the interface
    id: 0,  
    title: '',
    body: ''
  };

  isEdit: boolean = false;

  constructor(private postService : PostService) { }

  ngOnInit() {
    this.postService.getPosts().subscribe(posts => {
      this.posts = posts; // posts retrieved from Observable is stored in this.posts array
    });
  }

  onNewPost (post: Post) { // post coming from Event Emitter
    this.posts.unshift(post); // this is what user enters from a diff component, 
                              //  that will get added to this.posts array
  }

  editPost (post: Post) {
    this.currentPost = post;
    this.isEdit = true; // is being edited
  }

  // to move the post to the top after being edited
  onUpdatePost (post: Post) {
    this.posts.forEach((cur, index) => {
      if (post.id === cur.id) { // the post we are currently editing.. and if it matches any one of the posts on list visible
        this.currentPost = post;
        this.posts.splice(index, 1); // cuts it out and moves to index 1
        this.posts.unshift(post);
        this.isEdit = false;
        this.currentPost = { // reset form
          id: 0,
          title: '',
          body: ''
        }
      }
    });
    
  }

  deletePost (post: Post) {
    if (confirm('Are you sure?')) { // confirmation message
      this.posts.forEach((cur, index) => {
        if (post.id === cur.id) { // the post we are currently editing.. and if it matches any one of the posts on list visible
          this.posts.splice(index, 1); // cuts it out and moves to index 1
          this.currentPost = { // reset form
            id: 0,
            title: '',
            body: ''
          }
        }
      });
    };
  }



}
