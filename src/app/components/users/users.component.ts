import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/models/User';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  user : User = { // helper User that will be reset 
    firstName : '',
    lastName : '', 
    email : ''
  };
  users : User[]; // stores all users
  showExtended : boolean = false;
  loaded : boolean = false;
  enableAdd : boolean = true;
  showForm : boolean = false;
  // extraClasses : {};
  // extraStyling : {};

  @ViewChild('userForm',{static: false}) form : any;
  // 'userForm' is from the name of the form from .html file!
  // must import @ViewChild!

  // injecting service as dependency
  // private means can only use within this class - usually private
  constructor(private dataService : UserService) { }

  ngOnInit() {
    // assign users from Service to this.users array

    // in users.component.ts : not working because now using Observable
    // this.users = this.dataService.getUsers();

    // so change to 
    this.dataService.getUsers().subscribe( users => {
      this.users = users;
    });

    this.loaded = true;
  }

  onSubmit ({value, valid} : {value : User, valid : boolean}) {
    if (!valid) {
      console.log('Form not valid');
    } else {
      value.isActive = true;
      value.registered = new Date ();
      value.hide = true;
      // this.users.unshift(value);
      this.dataService.addUsers(value);
      this.form.reset();
    }
  }

  /*addUser () {
    // setting other user data when adding them (not from form)
    this.user.isActive = true;
    this.user.registered = new Date ();

    this.users.unshift(this.user); 
    // unshift to add from beginning of array

    this.user = { // resets user helper variable
      firstName : '',
      lastName : '', 
      email : ''
    };
  }*/

  // setExtraClasses () {
  //   this.extraClasses = {
  //     'theRed' : this.enableAdd 
  //   }
  // }

  // setExtraStyling () {
  //   this.extraStyling = {
  //     'padding-top' : this.showExtended ? '0' : '60px'
  //   }
  // }
  




}
